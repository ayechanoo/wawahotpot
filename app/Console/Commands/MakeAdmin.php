<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Illuminate\Support\Facades\Hash;

class MakeAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->ask('Enter your name!');
        $email = $this->ask('Enter your email!');
        $password = $this->ask('Enter your password (8 chars)');
        $confirmed = $this->ask('Enter password again for confirm');

        if ($password !== $confirmed) {

            $this->error('Passwords did not match.');
        } else {

            try {
                
               $user =  User::create(
                    [
                        'name' => $name,
                        'email' => $email,
                        'password' => Hash::make($password),
                    ]
                );
                $user->assignRole('Admin');

                $this->info("Admin was successfully registered.");

            } catch (\Exception $e) {
                \Log::error('Create Admin Exception :' . $e);
                $this->error('Oops! Something went wrong! Please try again later!');
            }
        }

        
    }
}
